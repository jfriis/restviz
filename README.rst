=======
restviz
=======


.. image:: https://img.shields.io/pypi/v/restviz.svg
        :target: https://pypi.python.org/pypi/restviz

.. image:: https://img.shields.io/travis/jfriis/restviz.svg
        :target: https://travis-ci.com/jfriis/restviz

.. image:: https://readthedocs.org/projects/restviz/badge/?version=latest
        :target: https://restviz.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Vizualizing restries


* Free software: GNU General Public License v3
* Documentation: https://restviz.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
