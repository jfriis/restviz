"""Top-level package for restviz."""

__author__ = """Jens Friis-Nielsen"""
__email__ = 'jensfriisnielsen@gmail.com'
__version__ = '0.1.0'
